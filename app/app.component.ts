// TO DO- try to refactor how initial language and country is set to use currentlanguage and currentcountry services
import { Component, Input, Output, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AfterViewInit } from '@angular/core'
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { CurrentLanguageService } from './services/currentlanguage.service';
import { CurrentCountryService } from './services/currentcountry.service';
import { CountriesService } from './services/countries.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {



  constructor(private translate: TranslateService, private modalService: NgbModal) {
    translate.addLangs(["English", "French"]);
    translate.setDefaultLang('English');
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/English|French/) ? browserLang : 'English');
  }

  ngAfterViewInit() {
    let myCountry = Cookie.get('CountrySelected');
    let myLanguageJson = Cookie.get('LanguageJsonSelected');
    if (myCountry == null || myLanguageJson == null) {
      this.open();
    }

    if (myLanguageJson != null) {
      this.translate.use(myLanguageJson);
    }
  }
  open() {
    const modalRef = this.modalService.open(NgbdModalContent);
  }
}

@Component({
  selector: 'ngbd-modal-content',
  template: `

  <div class="modal-header">
     <h4 class="modal-title" *ngIf="!this.isLanguageCookieSet || !this.isCountryCookieSet">Welcome to the New FAA Program Simulator</h4>
     <h4 class="modal-title" *ngIf="this.isLanguageCookieSet && this.isCountryCookieSet">Settings</h4>
     <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
     <span aria-hidden="true">&times;</span>
     </button>
  </div>
  <div class="modal-body">
     <form>
        <div class="form-group">
           <label for="">Select language:</label>
           <select [ngModel]="selectedLanguage" (ngModelChange)="onLanguageChange($event)" name="sel1" class="form-control">
           <option [value]="language" *ngFor="let language of listoflanguages">{{language}}</option>
           </select>
        </div>
        <div class="form-group">
           <label for="">Select the country for your primary business:</label>
           <select [ngModel]="selectedCountry"  (ngModelChange)="onCountryChange($event)" name="sel2" class="form-control">
            <option [value]="country.AmwayCountry" *ngFor="let country of listofcountries">{{country.AmwayCountry}}</option>

           </select>
        </div>
     </form>
  </div>
  <div class="modal-footer">
     <button type="button" class="btn btn-primary" (click)="modalSave()">
      <span *ngIf="!this.isLanguageCookieSet || !this.isCountryCookieSet">Next</span>
      <span *ngIf="this.isLanguageCookieSet && this.isCountryCookieSet">Save Changes</span>
    </button>
  </div>

  `
})

export class NgbdModalContent implements OnInit {
  closeResult: string;
  public CurrencyConversionRate;
  listoflanguages:  string[];
  listofcountries:  string[];
  listOfCountriesFromService: any;
  selectedLanguage: any;
  selectedCountry:  string;
  isLanguageCookieSet: boolean;
  isCountryCookieSet: boolean;
  jsonFile: string;

  constructor(private translate: TranslateService,
    public activeModal: NgbActiveModal,
    private countriesService: CountriesService,
    private currentLanguageService: CurrentLanguageService,
    public currentCountryService: CurrentCountryService ) {
      this.isLanguageCookieSet = Cookie.get('LanguageSelected') ? true : false;
      this.isCountryCookieSet = Cookie.get('CountrySelected') ? true : false;
      this.selectedLanguage = this.isLanguageCookieSet ? Cookie.get('LanguageSelected') : 'English';
      this.selectedCountry  = this.isCountryCookieSet ? Cookie.get('CountrySelected') : 'United States';
      this.listoflanguages  = ['Chinese (Simplified)','Chinese (Traditional)','English','German','Japanese','Korean','Russian','Spanish (Latin America)','Spanish (Spain)','Thai','Turkish','Ukrainian'];
      this.listofcountries  = [];

      this.listOfCountriesFromService = this.countriesService.getCountries().subscribe(message => {
        this.listOfCountriesFromService = message;
         this.listofcountries = this.listOfCountriesFromService.countries;

      });
  }

  ngOnInit(): void {
    // set cookies to English and United States by default if none are present
    if (!this.isLanguageCookieSet) {
      this.setLanguage('English');
    }
    if (!this.isCountryCookieSet) {
      this.setCountry('United States');
    }
  }

  onCountryChange(newValue) {
    this.selectedCountry = newValue;
  }

  onLanguageChange(newValue) {
    this.selectedLanguage = newValue;
  }

  modalSave(): void {
    this.setLanguage(this.selectedLanguage);
    this.setCountry(this.selectedCountry);
    this.activeModal.close();
  }

  setLanguage(newValue): void {
    // remove parentheses and convert spaces to hyphens
    this.jsonFile = newValue.replace(/[()]/g, '').replace(/\s+/g, '-');

    // send message to subscribers via observable subject
    this.currentLanguageService.setLanguage(newValue);

    this.currentLanguageService.setLanguageJson(this.jsonFile);

    this.translate.use(this.jsonFile);
  }

  setCountry(newValue): void {
    // send message to subscribers via observable subject
    this.currentCountryService.setCountry(newValue);
  }
}


