import { Component, Input, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Subscription }   from 'rxjs/Subscription';

import { CurrentCountryService } from '../../services/currentcountry.service';
import { CountriesService } from '../../services/countries.service';
import { CurrentLanguageService } from '../../services/currentlanguage.service';

@Component({
  selector: 'projected-bonus',
  templateUrl: './projectedbonus.component.html'
})
export class ProjectedBonus implements OnDestroy {

  @Input() ProjectedMultiplier;
  @Input() ProjectedBonus;
  @Input() MonthlyBonus;

  @Output() change: EventEmitter<number> = new EventEmitter<number>();


  public superlegs;
  public enteredFFAPoints;
  public enteredSliderValue;
  public superLegsLessthan5;
  public superLegsMorethan5;
  public currencyconvertor;

  myConversionFactor;
  myCurrencyCode;
  myCurrencyPrefix;
  myCurrencySuffix;
  myCurrencyNumberSeparator;
  myCurrencyDecimalSeparator;
  currentLanguage: string;
  subscriptionToLanguageUpdates: Subscription;
  subscriptionToCountryUpdates: Subscription;
  subscriptionToCountryCodeUpdates: Subscription;
  subscriptionToCurrencyPrefixUpdates: Subscription;
  subscriptionToCurrencySuffixUpdates: Subscription;
  subscriptionToCurrencyNumberSeparatorUpdates: Subscription;
  subscriptionToCurrencyDecimalSeparatorUpdates: Subscription;

  constructor(private currentCountryService: CurrentCountryService,
              private currentLanguageService: CurrentLanguageService){

    this.subscriptionToCountryUpdates = this.currentCountryService.getCurrencyConversion().subscribe(message => this.myConversionFactor = message.conversionFactor);
    this.subscriptionToCountryCodeUpdates = this.currentCountryService.getCurrencyCode().subscribe(message => this.myCurrencyCode = message.currencyCode);
    this.subscriptionToCurrencyPrefixUpdates = this.currentCountryService.getCurrencyPrefix().subscribe(message => this.myCurrencyPrefix = message.currencyPrefix);
    this.subscriptionToCurrencySuffixUpdates = this.currentCountryService.getCurrencySuffix().subscribe(message => this.myCurrencySuffix = message.currencySuffix);
    this.subscriptionToCurrencyNumberSeparatorUpdates = this.currentCountryService.getCurrencyNumberSeparator().subscribe(message => this.myCurrencyNumberSeparator = message.currencyNumberSeparator);
    this.subscriptionToCurrencyDecimalSeparatorUpdates = this.currentCountryService.getCurrencyDecimalSeparator().subscribe(message => this.myCurrencyDecimalSeparator = message.currencyDecimalSeparator);

    // subscribe to language updates
    this.subscriptionToLanguageUpdates = this.currentLanguageService.getLanguage().subscribe(message => this.currentLanguage = message.language.toLowerCase());

    this.superlegs = parseInt(Cookie.get('SuperLegs'));
    this.enteredFFAPoints = parseFloat(Cookie.get('FAAPoints'));
    this.enteredSliderValue = parseInt(Cookie.get('SliderValue'));

    if(this.enteredFFAPoints < 150 || this.enteredSliderValue < 12)
    {
       this.superLegsLessthan5 = 0;
       this.superLegsMorethan5 = 0;
    }
    else{

    if(this.superlegs > 5){
    this.superLegsLessthan5 = 5;
    this.superLegsMorethan5 = this.superlegs - this.superLegsLessthan5;
  }
  else if(this.superlegs <=5 && this.superlegs >= 2){
     this.superLegsLessthan5 = this.superlegs;
      this.superLegsMorethan5 = 0;
  }
  else{
      this.superLegsLessthan5 = 0;
      this.superLegsMorethan5 = 0;
  }
    }

  }

 afterViewInit() {
    this.myConversionFactor = this.currentCountryService.getCurrencyConversion();
    this.myCurrencyCode = this.currentCountryService.getCurrencyCode();
    this.myCurrencyCode = this.currentCountryService.getCurrencyCode();
    this.myCurrencyPrefix = this.currentCountryService.getCurrencyPrefix();
    this.myCurrencySuffix = this.currentCountryService.getCurrencySuffix();
    this.myCurrencyNumberSeparator = this.currentCountryService.getCurrencyNumberSeparator();
    this.myCurrencyDecimalSeparator = this.currentCountryService.getCurrencyDecimalSeparator();
  }

  ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.subscriptionToCountryUpdates.unsubscribe();
  }
}
