import {Component, Input, Inject, forwardRef} from '@angular/core';

import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { BonusSimulator } from '../../components/BonusSimulator/bonussimulator.component';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h3 class="modal-title">{{ 'reset-modal.faa-43-4' | translate }}</h3>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>{{ 'reset-modal.faa-43-1' | translate }}</p>
    </div>
    <div class="modal-footer reset-simulator-modal-footer">
      <button type="button" class="btn btn-default reset-simulator-btn" (click)="activeModal.dismiss()">{{ 'reset-modal.faa-43-2' | translate }}</button>
       <button type="button" class="btn btn-default reset-simulator-btn background-amway-blue text-white" (click)="resetSimulator();">{{ 'reset-modal.faa-43-3' | translate }}</button>
    </div>
  `
})
export class ResetModalContent {
  @Input() name;
  public componentToDisplay:number = 1;

  constructor(public activeModal: NgbActiveModal) {

  }
 resetSimulator(){
    Cookie.delete('CurrencyConversionRate');
    Cookie.delete('FAAPoints');
    Cookie.delete('SliderValue');
    Cookie.delete('SuperLegs');
    Cookie.delete('ConversionFactor');
    Cookie.delete('CountrySelected');
    Cookie.delete('CurrencyCode');
    Cookie.delete('CurrencyDecimalSeparator');
    Cookie.delete('CurrencyNumberSeparator');
    Cookie.delete('CurrencyPrefix');
    Cookie.delete('CurrencySuffix');
    Cookie.delete('LanguageSelected');
    location.reload();
  }

}

@Component({
  selector: 'reset-simulator',
  templateUrl: 'resetsimulator.component.html'
})
export class ResetSimulatorComponent {
  public componentToDisplay:number = 1;

  constructor(private modalService: NgbModal) {}

  open() {
    const modalRef = this.modalService.open(ResetModalContent);
    modalRef.componentInstance.name = 'Reset';
  }
}