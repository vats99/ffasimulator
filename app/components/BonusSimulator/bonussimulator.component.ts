import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ScrollToModule } from 'ng2-scroll-to';
import { Subscription }   from 'rxjs/Subscription';

import { CurrentCountryService } from '../../services/currentcountry.service';
import { CountriesService } from '../../services/countries.service';
import { CurrentLanguageService } from '../../services/currentlanguage.service';

@Component({
  selector: 'bonus-simulator',
  templateUrl: './bonussimulator.component.html'
})
export class BonusSimulator {
  public isCollapsed = false;
  public componentToDisplay:number = 1;
  public sliderValue;
  public ffaPoints;
  public SuperLegs;
  public projected;
  public sliderValueArray;
  public fplatArray;
  public ebrArray;
  public dbrArray;
  public fplats: Number;
  public dbr: Number;
  public ebr: Number;
  public gmcBonus;
  public isSimpleFaaDirty: Boolean;
  private rangeMin: Number;
  private rangeMax: Number;
  public isSimpleFaaAboveLowThreshold: Boolean;
  public isSimpleFaaWithinRange: Boolean;
  public faaPoint=0;
  public totalFaaPoint=0;
  public superlegInputByLeg=0;
  public totalSuperlegInputByLeg=0;
  public ffaPointForSuperLeg;
  public finalsuperleg;
  public faaPointsSelectedTab;
  myCurrencyCode;
  myCurrencyPrefix;
  myCurrencySuffix;
  myCurrencyNumberSeparator;
  myCurrencyDecimalSeparator;
  currentLanguage: string;

  subscriptionToCountryCodeUpdates: Subscription;
  subscriptionToCurrencyPrefixUpdates: Subscription;
  subscriptionToCurrencySuffixUpdates: Subscription;
  subscriptionToCurrencyNumberSeparatorUpdates: Subscription;
  subscriptionToCurrencyDecimalSeparatorUpdates: Subscription;
  subscriptionToLanguageUpdates: Subscription;

 constructor( private currentCountryService: CurrentCountryService,
              private currentLanguageService: CurrentLanguageService){
   this.subscriptionToCountryCodeUpdates = this.currentCountryService.getCurrencyCode().subscribe(message => this.myCurrencyCode = message.currencyCode);
   this.subscriptionToCurrencyPrefixUpdates = this.currentCountryService.getCurrencyPrefix().subscribe(message => this.myCurrencyPrefix = message.currencyPrefix);
   this.subscriptionToCurrencySuffixUpdates = this.currentCountryService.getCurrencySuffix().subscribe(message => this.myCurrencySuffix = message.currencySuffix);
   this.subscriptionToCurrencyNumberSeparatorUpdates = this.currentCountryService.getCurrencyNumberSeparator().subscribe(message => this.myCurrencyNumberSeparator = message.currencyNumberSeparator);
   this.subscriptionToCurrencyDecimalSeparatorUpdates = this.currentCountryService.getCurrencyDecimalSeparator().subscribe(message => this.myCurrencyDecimalSeparator = message.currencyDecimalSeparator);

   // subscribe to language updates
   this.subscriptionToLanguageUpdates = this.currentLanguageService.getLanguage().subscribe(message => this.currentLanguage = message.language.toLowerCase());

   this.projected = {
    "Multiplier": 0,
    "monthlyBonus":0
   }
   this.fplats = 0;
   this.ebr = 0;
   this.dbr = 0;
   this.rangeMin = 20;
   this.rangeMax = 999;
   this.isSimpleFaaAboveLowThreshold = false;
   this.isSimpleFaaWithinRange = false;
   this.isSimpleFaaDirty = false;

 }
 afterViewInit() {
    this.myCurrencyCode = this.currentCountryService.getCurrencyCode();
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscriptionToCountryCodeUpdates.unsubscribe();
    this.subscriptionToCurrencyPrefixUpdates.unsubscribe();
    this.subscriptionToCurrencySuffixUpdates.unsubscribe();
    this.subscriptionToCurrencyNumberSeparatorUpdates.unsubscribe();
    this.subscriptionToCurrencyDecimalSeparatorUpdates.unsubscribe();
    this.subscriptionToLanguageUpdates.unsubscribe();
  }

 ngOnInit() {
   this.SuperLegs = 0;
   // set slider value to default value if no cookie has been set
   if ( !Cookie.get('SliderValue') ) {
     Cookie.set('SliderValue', '4');
   }
  }

  setSelectedFaaTab(event: any) {
    this.faaPointsSelectedTab = event.nextId;
  }

 emitSliderValue(e){
  // reset FAA Points quick input if slider value changes
  this.ffaPoints = this.sliderValue === e ? this.ffaPoints : null;
  // if this.ffaPoints is set to null, the error should no longer be visible
  this.isSimpleFaaDirty = this.ffaPoints != null ? this.isSimpleFaaDirty : false;
  // if this.ffaPoints is set to null, the quick input no longer has a valid value
  this.isSimpleFaaAboveLowThreshold = this.ffaPoints != null ? this.isSimpleFaaAboveLowThreshold : false;
  this.isSimpleFaaWithinRange = this.isSimpleFaaAboveLowThreshold;

  this.sliderValue = e;
  this.sliderValueArray = new Array(this.sliderValue);
  // if the value has changed, create a blank array
  this.fplatArray = Array(this.sliderValue);

  this.ebrArray = Array(this.sliderValue);
  this.dbrArray = Array(this.sliderValue);
  this.totalFaaPoint = 0;
}

totalValueChanged(e,n){
  this.totalFaaPoint = 0;
  for( let i = 0; i< document.getElementsByClassName("faa-points-table-point-tracking").length; i++){

    // To Do: this is bad. Make this more Angular and less jQuery
    let myInnerHtml = document.getElementsByClassName("faa-points-table-point-tracking")[i].innerHTML;
    // change comma to decimal for calcuation if necessary
    myInnerHtml = myInnerHtml.indexOf(',') === -1 ? myInnerHtml : myInnerHtml.replace(/,/g , ".");

    this.faaPoint = parseFloat(myInnerHtml);

    this.totalFaaPoint = this.totalFaaPoint + this.faaPoint;
  }

}

addMoreLeg(){
  this.sliderValueArray.length = this.sliderValueArray.length+1;
}


updateBonus(e){
  this.projected.monthlyBonus = e;
}


setComponent(componentNumber: number): void {
  this.componentToDisplay = componentNumber;

  //Counting SuperLEgs for by SuperLegs

  this.totalSuperlegInputByLeg = 0;
  for( let i = 0; i< document.getElementsByClassName("faa-points-table-point-tracking").length; i++){
    this.ffaPointForSuperLeg = parseFloat(document.getElementsByClassName("faa-points-table-point-tracking")[i].innerHTML);
    if(this.ffaPointForSuperLeg == 30){
      this.totalSuperlegInputByLeg ++;
    }
  }



  //Projected Multiplier
  if(componentNumber == 3) {
     if(this.faaPointsSelectedTab == 'tab-input-by-leg'){
      this.ffaPoints =  this.totalFaaPoint;
        if(this.totalFaaPoint >=150){
            this.finalsuperleg = this.totalSuperlegInputByLeg;
            Cookie.set('SuperLegs',this.finalsuperleg);
      }
     } else {
      this.ffaPoints = this.ffaPoints;
       Cookie.set('SuperLegs', this.SuperLegs);
     }
      //FAA POINTS LESS THAN 20
      if(this.ffaPoints < 20 ){
          this.projected.Multiplier = 0;
      }
      //FAA POINTS 20 - 26
      else if(this.ffaPoints >=20 && this.ffaPoints < 27){

              this.projected.Multiplier = 200;
      }
      //FAA POINTS 27 - 34
      else if(this.ffaPoints >=27 && this.ffaPoints < 35){
          this.projected.Multiplier = 250;
      }

      //FAA POINTS 35 - 44
       else if(this.ffaPoints >=35 && this.ffaPoints < 45){
          this.projected.Multiplier = 300;
      }

      //FAA POINTS 45 - 59
       else if(this.ffaPoints >=45 && this.ffaPoints < 60){
         if(this.sliderValue < 8)
          this.projected.Multiplier = 300;
          else{
            this.projected.Multiplier = 400;
          }
      }
      //FAA POINTS 60 - 74
      else if(this.ffaPoints >=60 && this.ffaPoints < 75){
         if(this.sliderValue < 8)
          this.projected.Multiplier = 300;
          else{
            this.projected.Multiplier = 450;
          }
      }

      //FAA POINTS 75 - 89
      else if(this.ffaPoints >=75 && this.ffaPoints < 90){
         if(this.sliderValue < 8)
          this.projected.Multiplier = 300;
          else{
            this.projected.Multiplier = 500;
          }
      }
      //FAA POINTS 90 - 104
      else if(this.ffaPoints >=90 && this.ffaPoints < 105){
         if(this.sliderValue < 10 && this.sliderValue >= 8){
          this.projected.Multiplier = 500;
         }
          else if(this.sliderValue < 8){
            this.projected.Multiplier = 300;
          }
          else{
            this.projected.Multiplier = 600;
          }
      }
      //FAA POINTS 105 - 124
      else if(this.ffaPoints >=105 && this.ffaPoints < 125){
         if(this.sliderValue < 10 && this.sliderValue >= 8){
          this.projected.Multiplier = 500;
         }
          else if(this.sliderValue < 8){
            this.projected.Multiplier = 300;
          }
          else{
            this.projected.Multiplier = 650;
          }
      }

      //FAA POINTS 124 - 149
      else if(this.ffaPoints >=125 && this.ffaPoints < 150){
         if(this.sliderValue < 10 && this.sliderValue >= 8){
          this.projected.Multiplier = 500;
         }
          else if(this.sliderValue < 8){
            this.projected.Multiplier = 300;
          }
          else{
            this.projected.Multiplier = 700;
          }
      }
      //FAA POINTS MORE THAN 150
      else if(this.ffaPoints >=150){
         if(this.sliderValue < 12 && this.sliderValue >= 10){
           this.projected.Multiplier = 700;
         }
         else if(this.sliderValue < 10 && this.sliderValue >= 8){
          this.projected.Multiplier = 500;
         }
          else if(this.sliderValue < 8){
            this.projected.Multiplier = 300;
          }
          else{
            this.projected.Multiplier = 750;

          }
      }
     Cookie.set('FAAPoints', this.ffaPoints);
    }
  }

  checkIsSimpleFaaAboveLowThreshold (fieldValue: number): void {
    this.isSimpleFaaAboveLowThreshold = fieldValue >= this.rangeMin;
    this.isSimpleFaaWithinRange = this.isSimpleFaaAboveLowThreshold;
  }

  checkIsSimpleFaaWithinRange (fieldValue: number, myInput): void {
    this.isSimpleFaaDirty = myInput.dirty;
    this.isSimpleFaaWithinRange = fieldValue >= this.rangeMin && fieldValue <= this.rangeMax;
    if((fieldValue < 150)){
      this.SuperLegs = 0;
    }
  }
}
