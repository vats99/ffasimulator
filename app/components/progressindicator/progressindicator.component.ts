import { Component, Input } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';

@Component({
  selector: 'progress-indicator',
  templateUrl: './progressindicator.component.html'
})

export class ProgressIndicator {
  @Input() currentStep: number;

  isComplete(myStepNumber: number): boolean {
    return myStepNumber < this.currentStep;
  }

  isInactive(myStepNumber: number): boolean {
    return myStepNumber > this.currentStep;
  }

}
