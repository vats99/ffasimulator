import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'welcome-banner',
  templateUrl: './welcomebanner.component.html'
})
export class WelcomeBannerComponent {
  closeResult: string;
  videoUrl: SafeUrl;

  constructor(private modalService: NgbModal,
              private translate: TranslateService,
              private sanitizer: DomSanitizer) {  }

  open(content) {
    // 1. get translated video-url
    // 2. sanitize and return it to iframe
    this.translate.get('global.video-url').subscribe((res: string) => {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(res);
    });

    this.modalService.open(content, { 'size': 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
