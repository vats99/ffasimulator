import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'founder-platinum-legs-slider',
  templateUrl: './founderplatinumlegsslider.component.html'
})

export class FounderPlatinumLegsSlider implements OnInit {
  @Output() onSliderUpdate = new EventEmitter();
  @Input() faaPointsSelectedTab: string;

  maxFPLRange: number = 12;
  minFPLRange: number = 4;
  fplSliderValue: any = Cookie.get('SliderValue');
  fplDisplayValue: string;

  ngOnInit(): void {
    // faaPointsSelectedTab only gets set if user has interacted with the tabs on step 2.
    // This lets us know if this is the initial first step or if the user has arrived on step 1 from step 2
    if( !this.faaPointsSelectedTab ) {
      this.updateDisplay(this.fplSliderValue);
    }
  }

  isMin() {
    // are we at the bottom of the availble range?
    return this.fplSliderValue <= this.minFPLRange;
  }

  isMax() {
    // are we at the top of the availble range?
    return this.fplSliderValue >= this.maxFPLRange;
  }

  isCurrentValueEqual(hashmarkValue: number) {
    return hashmarkValue === Number(this.fplSliderValue);
  }

  incrementValue() {
    // add one to the value
    this.fplSliderValue  = this.fplSliderValue + 1 >= this.maxFPLRange ? this.maxFPLRange : this.fplSliderValue + 1;
    this.updateDisplay(this.fplSliderValue);
  }

  decrementValue() {
    // subtract one from the value
    this.fplSliderValue  = this.fplSliderValue - 1 <= this.minFPLRange ? this.minFPLRange : this.fplSliderValue - 1;
    this.updateDisplay(this.fplSliderValue);
  }

  updateDisplay(value: number) {
    // update FPL number on change
    // add + to display if the highest value is selected
    this.fplDisplayValue = value >= this.maxFPLRange ? String(value) + '+' :  value < this.minFPLRange ? String(this.minFPLRange) : String(value);

    // Note: when value comes from dragging the range element, value is a string for unknown reasons
    this.fplSliderValue = Number(value);

    //Simulator Calculation Script
    this.onSliderUpdate.emit(this.fplSliderValue);
    Cookie.set('SliderValue', this.fplSliderValue);

  }
}
