import { Component, Input, Inject, AfterViewInit, forwardRef, OnDestroy } from '@angular/core';
import { AppComponent, NgbdModalContent } from '../../app.component';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription }   from 'rxjs/Subscription';

import { CurrentLanguageService } from '../../services/currentlanguage.service';
import { CurrentCountryService } from '../../services/currentcountry.service';

@Component({
  selector: 'header-nav',
  templateUrl: './headernav.component.html'
})

export class HeaderComponent implements OnDestroy {
  myLanguage: any;
  language: any;
  myCountry: any;
  country: any;
  subscriptionToLanguageUpdates: Subscription;
  subscriptionToCountryUpdates: Subscription;

  constructor( @Inject(forwardRef(() => AppComponent)) private _parent:AppComponent,
              private modalService: NgbModal,
              private currentLanguageService: CurrentLanguageService,
              private currentCountryService: CurrentCountryService ) {

    // subscribe to language updates
    this.subscriptionToLanguageUpdates = this.currentLanguageService.getLanguage().subscribe(message => this.myLanguage = message.language);

    // subscribe to country updates
    this.subscriptionToCountryUpdates = this.currentCountryService.getCountry().subscribe(message => this.myCountry = message.country);
  }

  afterViewInit() {
    //this.myCountry = Cookie.get('CountrySelected');
    this.myLanguage = this.currentLanguageService.getLanguage();
    this.myCountry = this.currentCountryService.getCountry();
   
  }

  ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.subscriptionToLanguageUpdates.unsubscribe();
      this.subscriptionToCountryUpdates.unsubscribe();
  }

  open() {
    const modalRef = this.modalService.open(NgbdModalContent);
  }

}
