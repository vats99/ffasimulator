// TO DO: convert the currencyPipe.transform parameters to an object
import { Directive, HostListener, ElementRef, Input, OnInit } from "@angular/core";
import { MyCurrencyPipe } from "../pipes/myCurrency.pipe";

@Directive({
  selector: "[myCurrencyFormatter]"
})
export class MyCurrencyFormatterDirective implements OnInit {
  @Input() prefix: string = '$';
  @Input() numberSeparator: string = ',';
  @Input() decimalSeparator: string = '.';
  @Input() numberOfDecimalPlaces: string = '2';
  @Input() suffix: string = '';
  private el: HTMLInputElement;

  constructor(
    private elementRef: ElementRef,
    private currencyPipe: MyCurrencyPipe
  ) {
    this.el = this.elementRef.nativeElement;
  }

  // TO DO: this.el.value does not have a value when this fires.
  // ngDoCheck does fire when the model has changed the value initially, but also fires many times subsequently
  ngOnInit() {
    this.el.value = this.currencyPipe.transform(this.el.value,
                                                this.prefix,
                                                this.numberSeparator,
                                                this.decimalSeparator,
                                                this.numberOfDecimalPlaces,
                                                this.suffix);
  }

  @HostListener("focus", ["$event.target.value"])
  onFocus(value) {
    // reverse the transform.
    // pass number of decimal places and decimal separator so non-US number format doesn't cause the number to be mangled
    this.el.value = this.currencyPipe.parse(value,
                                            this.numberOfDecimalPlaces,
                                            this.numberSeparator,
                                            this.decimalSeparator);
  }

  @HostListener("blur", ["$event.target.value"])
  onBlur(value) {
    this.el.value = this.currencyPipe.transform(this.el.value,
                                                this.prefix,
                                                this.numberSeparator,
                                                this.decimalSeparator,
                                                this.numberOfDecimalPlaces,
                                                this.suffix);
  }
}
