import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'currencyformat'})
export class CurrencyFormat implements PipeTransform {
  transform(value): any {
    return value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}