import { Pipe, PipeTransform } from '@angular/core';

const PADDING = '000000';

@Pipe({ name: 'myCurrency'
//         , pure: false
      })

export class MyCurrencyPipe implements PipeTransform {

  constructor() {
  }

  // {{'39292911.223' | myCurrency: '$':',':'.':'2':'' }}
  // results in $39,292,911.22
  // 1. prefix- could be an empty string or a currency symbol string
  // 2. numberSeparator - period, comma, other
  // 3. decimalSeparator- period, comma, other
  // 4. numberOfDecimalPlaces
  // 5. suffix- could be an empty string or a currency symbol string
  transform(value: number | string,
            prefix: string = '$',
            numberSeparator: string = ',',
            decimalSeparator: string = '.',
            numberOfDecimalPlaces: number | string = '0',
            suffix: string = '',
            omitDecimalZeroes: boolean = false,
            isFaaPoints: boolean = false,
            isNumberLocalized: boolean = true ): string {

    // if a number is not localized and the decimalSeparator is something other than a period (like a comma),
    // replace the period with the localized decimalSeparator before formatting
    value = isNumberLocalized === false && decimalSeparator !== '.' ? (value || '').toString().replace(/\./g, decimalSeparator) : value;

    let [ integer, fraction = '' ] = (value || '').toString()
      .split(decimalSeparator);

    // TO DO: the FAA points formatting should probably be its own pipe OR the name of this pipe should be changed to something more general,
    // like numberFormatter
    //
    // FAA points are not currency and only resolve to tenths.
    // It is possible that a number like 1,5 (1.5 in USA terms) could get passed, so we need to catch the comma
    if ( isFaaPoints ) {
      // if decimal separator isn't a period, change it to a period to do the math
      let faaValue = value.toString().split(/[,\.]/);
      if (faaValue.length > 1) {
        return faaValue[0] + decimalSeparator + faaValue[1];
      } else {
        return faaValue[0];
      }
    }

    fraction = ( Number(numberOfDecimalPlaces) > 0 && Number(fraction) > 0 )
        ? decimalSeparator + (fraction + PADDING).substring(0, Number(numberOfDecimalPlaces))
        : '';

    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, numberSeparator);

    return prefix + integer + fraction + suffix;
  }

  // because number separators and decimal separators vary from country to country,
  // parse needs to know what to look for
  parse(value: string,
        numberOfDecimalPlaces: string = '0',
        numberSeparator: string = ',',
        decimalSeparator: string = '.'): string {

    let [ integer, fraction = '' ] = (value || '').split(decimalSeparator);

    // if the numberSeparator is a period, need to escape it to prevent RexExp from borking.
    // using a switch in case characters other than periods need caught in the future

    switch (numberSeparator) {
      case '.':
        integer = integer.replace(/\./g, '')
        break;

      default:
        integer = integer.replace(new RegExp(numberSeparator, 'g'), '');
    }

    fraction = parseInt(fraction, 10) > 0 && Number(numberOfDecimalPlaces) > 0
      ? decimalSeparator + (fraction + PADDING).substring(0, Number(numberOfDecimalPlaces))
      : '';

    return integer + fraction;
  }

}
