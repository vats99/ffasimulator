import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ScrollToModule} from 'ng2-scroll-to';
import { CustomFormsModule } from 'ng2-validation'



import { AppComponent, NgbdModalContent } from './app.component';
import { HeaderComponent } from './components/header/headernav.component';
import { WelcomeBannerComponent } from './components/welcomebanner/welcomebanner.component';
import { ProjectedBonusCalculationDiagram } from './components/projectedbonuscalculationdiagram/projectedbonuscalculationdiagram.component';
import { FooterComponent } from './components/footer/footer.component';
import { FounderPlatinumLegsSlider } from './components/founderplatinumlegsslider/founderplatinumlegsslider.component';
import { BonusSimulator } from './components/BonusSimulator/bonussimulator.component';
import { ProgressIndicator } from './components/progressindicator/progressindicator.component';
import { ProjectedBonus } from './components/projectedbonus/projectedbonus.component';
import { ResetSimulatorComponent } from './components/resetsimulator/resetsimulator.component';
import { ResetModalContent } from './components/resetsimulator/resetsimulator.component';


import { CurrentLanguageService } from './services/currentlanguage.service';
import { CurrentCountryService } from './services/currentcountry.service';
import { CountriesService } from './services/countries.service';

import { CurrencyFormat } from './pipes/currencyFormatter.pipe';

import { MyCurrencyPipe } from "./pipes/myCurrency.pipe";
import {MyCurrencyFormatterDirective} from "./directives/myCurrencyformatter.directive";


import { OnlyNumber } from './directives/onlynumbers.directive';

export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, "../assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WelcomeBannerComponent,
    NgbdModalContent,
    ProjectedBonusCalculationDiagram,
    FooterComponent,
    FounderPlatinumLegsSlider,
    BonusSimulator,
    OnlyNumber,
    ProgressIndicator,
    ProjectedBonus,
    ResetSimulatorComponent,
    ResetModalContent,
    CurrencyFormat,
    MyCurrencyPipe,
    MyCurrencyFormatterDirective
  ],
  entryComponents: [NgbdModalContent, ResetModalContent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
    }),
    ScrollToModule.forRoot(),
    CustomFormsModule
  ],
  providers: [  CurrentLanguageService,
                CurrentCountryService,
                CountriesService,
                MyCurrencyPipe
                 ],
  bootstrap: [AppComponent]
})
export class AppModule { }
