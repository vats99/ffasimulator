// using BehaviorSubject instead of Subject so it will return the default value to subscribers immediately
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { CountriesService } from './countries.service';

@Injectable()
export class CurrentCountryService {
  private country = new BehaviorSubject<any>({ country: Cookie.get('CountrySelected') ? Cookie.get('CountrySelected') : 'United States' });
  private conversionFactor = new BehaviorSubject<any>({ conversionFactor: Cookie.get('ConversionFactor') ? Cookie.get('ConversionFactor') : 1 });

  private currencyCode = new BehaviorSubject<any>({ currencyCode: Cookie.get('CurrencyCode') ? Cookie.get('CurrencyCode') : 'USD' });
  private currencyPrefix = new BehaviorSubject<any>({ currencyPrefix: Cookie.get('CurrencyPrefix') ? Cookie.get('CurrencyPrefix') : '$' });
  private currencySuffix = new BehaviorSubject<any>({ currencySuffix: Cookie.get('CurrencySuffix') ? Cookie.get('CurrencySuffix') : '' });
  private currencyNumberSeparator = new BehaviorSubject<any>({ currencyNumberSeparator: Cookie.get('CurrencyNumberSeparator') ? Cookie.get('CurrencyNumberSeparator') : ',' });
  private currencyDecimalSeparator = new BehaviorSubject<any>({ currencyDecimalSeparator: Cookie.get('CurrencyDecimalSeparator') ? Cookie.get('CurrencyDecimalSeparator') : '.' });
  private currencySymbol = new BehaviorSubject<any>({ currencySymbol:Cookie.get('CurrencySymbol') ? Cookie.get('CurrencySymbol') : '$' });

  private listofcountries:  string[];
  private listOfCountriesFromService: any;
  private selectedCountry: any;

  constructor( private countriesService: CountriesService ) {
    // get country information for later use
    this.listOfCountriesFromService = this.countriesService.getCountries().subscribe(message => {
      this.listOfCountriesFromService = message;
      this.listofcountries = this.listOfCountriesFromService.countries;
      this.setCountry(Cookie.get('CountrySelected') ? Cookie.get('CountrySelected') : 'United States');
    });
  }

  // update cookie and observable
  setCountry(newCountry: string) {
    // find the country in the service and set its object as the selectedCountry
    for (let country in this.listOfCountriesFromService.countries) {
      if (this.listOfCountriesFromService.countries[country].AmwayCountry === newCountry) {
        this.selectedCountry = this.listOfCountriesFromService.countries[country];
        Cookie.set('CountrySelected', newCountry);
        this.country.next({ country: newCountry });
        this.setCurrencyConversion(this.selectedCountry);
      }
    }
  }

  // update  observable
  setCurrencyConversion(selectedCountry: any) {
    Cookie.set('ConversionFactor', selectedCountry.CurrencyConversionRate);
    this.conversionFactor.next({ conversionFactor: selectedCountry.CurrencyConversionRate });

    Cookie.set('CurrencyCode', selectedCountry.AlphabeticCode);
    this.currencyCode.next({ currencyCode: selectedCountry.AlphabeticCode});

    Cookie.set('CurrencyPrefix', selectedCountry.Prefix);
    this.currencyPrefix.next({ currencyPrefix: selectedCountry.Prefix});

    Cookie.set('CurrencySuffix', selectedCountry.Suffix);
    this.currencySuffix.next({ currencySuffix: selectedCountry.Suffix});

    Cookie.set('CurrencyNumberSeparator', selectedCountry.NumberSeparator);
    this.currencyNumberSeparator.next({ currencyNumberSeparator: selectedCountry.NumberSeparator});

    Cookie.set('CurrencyDecimalSeparator', selectedCountry.DecimalSeparator);
    this.currencyDecimalSeparator.next({ currencyDecimalSeparator: selectedCountry.DecimalSeparator});
  }

  // get observable value
  getCountry(): Observable<any> {
    return this.country.asObservable();
  }

  getCurrencyConversion(): Observable<any> {
    return this.conversionFactor.asObservable();
  }

  getCurrencyCode(): Observable<any> {
    return this.currencyCode.asObservable();
  }

  getCurrencyPrefix(): Observable<any> {
    return this.currencyPrefix.asObservable();
  }

  getCurrencySuffix(): Observable<any> {
    return this.currencySuffix.asObservable();
  }

  getCurrencyNumberSeparator(): Observable<any> {
    return this.currencyNumberSeparator.asObservable();
  }

  getCurrencyDecimalSeparator(): Observable<any> {
    return this.currencyDecimalSeparator.asObservable();
  }
}
