import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CountriesService {

  result: any;

  constructor(private http: Http) { }

  getCountries() {
    return this.http.get('assets/i18n/Countries.json')
       .map((res:Response) => res.json())
  }

};