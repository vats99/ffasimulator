// using BehaviorSubject instead of Subject so it will return the default value to subscribers immediately
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()
export class CurrentLanguageService {
  private language = new BehaviorSubject<any>({ language: Cookie.get('LanguageSelected') ? Cookie.get('LanguageSelected') : 'English' });
  private languageJson = new BehaviorSubject<any>({ languageJson: Cookie.get('LanguageJsonSelected') ? Cookie.get('LanguageJsonSelected') : 'English' });

  constructor() { }

  // update cookie and observable
  setLanguage(newLanguage: string) {
    Cookie.set('LanguageSelected', newLanguage);
    this.language.next({ language: newLanguage });
  }

  // get observable value
  getLanguage(): Observable<any> {
    return this.language.asObservable();
  }

  // update cookie and observable
  setLanguageJson(newLanguageJson: string) {
    Cookie.set('LanguageJsonSelected', newLanguageJson);
    this.languageJson.next({ language: newLanguageJson });
  }

  // get observable value
  getLanguageJson(): Observable<any> {
    return this.languageJson.asObservable();
  }
}